<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage Activities';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Manage Activities
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
									<th width="15%">Acitivity Date</th>
                                    <th width="15%">App In Diary</th>
                                    <th width="5%">Appt Sat</th>
									<th width="5%">Sales</th>
                                    <th width="10%">Cold Calls</th>
									<th width="15%">New Appts Made</th>
									<th width="15%">Conversion Rate Set to Sat</th>
									<th width="15%">Conversion Rate Sat to Sold</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $results = Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id']),'order' => 'd_entry_date DESC']);
                                if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) { ?>
                                        <tr>
                                            <td width="5%"><?= $row->d_id; ?></td>
											<td width="15%"><?= date('d-m-Y',strtotime($row->d_entry_date)); ?></td>
                                            <td width="15%"><?= $row->d_appt_set_diary; ?></td>
                                            <td width="10%"><?= $row->d_appt; ?></td>
											<td width="10%"><?= $row->d_sales; ?></td>
                                            <td width="10%"><?= $row->d_cold_calls_completed; ?></td>											
                                            <td width="15%"><?= $row->d_new_appt_made; ?></td>
											<td width="15%"><?= round($row->d_conversion_rate_set_sat); ?>%</td>
											<td width="15%"><?= round($row->d_conversion_rate_set_sold); ?>%</td>
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
