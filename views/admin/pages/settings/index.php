<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Settings';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Settings
            </h1>
        </section>
        <section class="content">
            <div class="row">
                        <!-- /.col -->
						<div class="col-md-4">
						  <!-- Widget: user widget style 1 -->
						  <div class="box box-widget widget-user">
							<!-- Add the bg color to the header using any of the bg-* classes -->
							<div class="widget-user-header bg-aqua-active">
							  <h3 class="widget-user-username"><?= $_SESSION['admin']['name']; ?></h3>
							  <h5 class="widget-user-desc"><?php if ($_SESSION['admin']['type']==0) echo "Admin"; else echo "Team Member"; ?></h5>
							</div>
							<div class="widget-user-image">
							  <img style="width: 90px;height: 90px;border: 3px solid #fff;object-fit: cover;" class="img-circle" src="<?php 
						 if(empty($_SESSION['admin']['ext'])){	
                                       echo $base_url."assets/admin/Ionicons/png/512/android-contact.png";											
                     				}else{
							               echo $base_url.$_SESSION['admin']['img'].'original.'.$_SESSION['admin']['ext'];
									}
						
						?>" alt="User Avatar">
							</div>
							
							<div class="box-footer">
							<?php if($_SESSION['admin']['type']!=0){?>
							  <div class="row text-center">
							  <span class="description-text">Current Month Details</span><br>
							  <span class="btn btn-primary btn-xs bg-aqua-active"><?= date('M-Y');?></span>
							  </div>
							<?php }?>
							  <div class="row">
								<div class="col-sm-4 border-right">
								  <div class="description-block">
								  <?php if($_SESSION['admin']['type']!=0){?>
									<h5 class="description-header">
									<?php
									 $activitySales=0;
                                     $activity=Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'])]);
                                     if(count($activity)>0){
									    foreach($activity as $s){
											$activityMonth=date("m", strtotime($s->d_entry_date));
											if($activityMonth==date('m')){
												$activitySales++;
											}
										}
                                       echo $activitySales;										
                                     }else echo "0"; 									 
 									?>
									</h5>
									<span class="description-text">Acitivities</span>
								  <?php }?>
								  </div>
								  <!-- /.description-block -->
								</div>
								<!-- /.col -->
								<div class="col-sm-4 border-right">
								  <div class="description-block">
								  <?php if($_SESSION['admin']['type']!=0){?>
									<h5 class="description-header">
									<?php
									 $countSales=0;
                                     $sale=SaleDetail::all(['conditions'=>array('s_user_id'=>$_SESSION['admin']['id'])]);
                                     if(count($sale)>0){
									    foreach($sale as $s){
											$saleMonth=date("m", strtotime($s->s_d_entry_date));
											if($saleMonth==date('m')){
												$countSales++;
											}
										}
                                       echo $countSales;										
                                     }else echo "0"; 									 
 									?>
									</h5>
									<span class="description-text">Sales</span>
								  <?php }?>
								  </div>
								  <!-- /.description-block -->
								</div>
								<!-- /.col -->
								<div class="col-sm-4">
								  <div class="description-block">
								  <?php if($_SESSION['admin']['type']!=0){?>
									<h5 class="description-header">
									<?php
                                     $saleCount=SaleDetail::all(['conditions'=>array('s_user_id'=>$_SESSION['admin']['id'])]);
                                     								 
 									?>
									</h5>
									<span class="description-text">Your Rank</span>
								  <?php }?>
								  </div>
								  <!-- /.description-block -->
								</div>
								<!-- /.col -->
							  </div>
							  <div class="row text-center">
							  <?= $_SESSION['admin']['email']; ?>
							  </div>
							  <div class="row text-center">
							  <span class="description-text">Member Since</span>
							  <?= $_SESSION['admin']['created_at']; ?>
							  </div>
							  <!-- /.row -->
							</div>
						  </div>
						  <!-- /.widget-user -->
						</div>
						<!-- /.col -->
						
                        <div class="col-lg-8">
						    <?php require_once $app_path . 'views/errors.php'; ?>
                                                   <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/settings?action=update">
                            
                                    <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input required type="text" class="form-control" name="name" id="name"
                                           placeholder="Enter Name" value="<?php echo $_SESSION['admin']['name']; ?>">
                                </div>
								<div class="form-group">
                                    <input required type="hidden" name="user_id"
                                            value="<?php echo $_SESSION['admin']['id']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input  type="password" minlength="6"  class="form-control" name="password"
                                              id="password"
                                              placeholder="Enter Password"/>
                                </div>
								
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text"  class="form-control" name="phone"
                                              id="phone"
                                              placeholder="Enter Phone"value="<?php echo $_SESSION['admin']['phone']; ?>" />
                                </div>							
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input  class="form-control" type="file" id="image" name="image">
                                </div>
                            </div>
                     <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                        
						
						</div> 						

								
                
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
