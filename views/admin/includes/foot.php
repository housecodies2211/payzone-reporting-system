<script src="<?= $base_url; ?>assets/admin/jquery/dist/jquery.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/raphael/raphael.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/moment/min/moment.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/fastclick/lib/fastclick.js"></script>
<script src="<?= $base_url; ?>assets/admin/ckeditor/ckeditor.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery_validation/dist/jquery.validate.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery_validation/dist/additional-methods.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= $base_url; ?>assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/select2/dist/js/select2.full.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/morris.js/morris.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/adminlte/js/adminlte.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/adminlte/js/pages/dashboard.js"></script>
<script src="<?= $base_url; ?>assets/admin/adminlte/js/demo.js"></script>
<script>
    $(function () {
        non_active = <?= isset($non_active)?$non_active:0; ?>;
        active = <?= isset($active)?$active:0; ?>;
        total = <?= isset($total)?$total:0; ?>;
        if($('#sales-chart').length > 0){
            var donut = new Morris.Donut({
              element: 'sales-chart',
              resize: true,
              colors: ["#DD4B39", "#00C0EF", "#00a65a"],
              data: [
                {label: "Non Active", value: non_active},
                {label: "Total", value: total},
                {label: "Active", value: active}
              ],
              hideHover: 'auto'
            });
        }
        $('.datepicker').datepicker({
            autoclose:true 
        });
        $('.select2').select2();
        $('.data_tables').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
        });
        $('.textarea').wysihtml5();

        $.widget.bridge('uibutton', $.ui.button);

        $("#form_validate").validate({
            errorClass: 'error_class',
            validClass: 'valid_class',
        });
        $("#activity_validate").validate({
            errorClass: 'error_class',
            validClass: 'valid_class',
        });
        setTimeout(function(){
            $('.alert').delay(1000).fadeOut(3500);
        },3000);
    });
    function delete_object(url) {
        var c = window.confirm("Are you sure, you want to delete it?");
        if (c) {
            window.location = url;
        }
    }
</script>
