<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Add User';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add User
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/user?action=add">
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input required type="text" class="form-control" name="name" id="name"
                                           placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input required type="email" class="form-control" name="email"
                                              id="email"
                                              placeholder="Enter Email" />
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input required type="password" minlength="6"  class="form-control" name="password"
                                              id="password"
                                              placeholder="Enter Password" />
                                </div>
                                <div class="form-group">
                                    <label for="confirm_pass">Confirm Password</label>
                                    <input data-rule-password="true" minlength="6" data-rule-equalTo="#password" required type="password"  class="form-control" name="confirm_pass"
                                              id="confirm_pass"
                                              placeholder="Enter Confirm Password" />
                                </div>								
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text"  class="form-control" name="phone"
                                              id="phone"
                                              placeholder="Enter Phone" />
                                </div>

                                <div class="form-group">
                                    <label for="designation">Designation</label>
                                    <select onchange="toggleUserType(this.value)" required class="form-control" id="designation" name="designation">
										<option value="0">
										Admin
										</option>
										<option value="1">
										User
										</option>
										
									</select>                                
								</div>
								<div class="form-group" id="usertypeContainer" style="display:none;">
                                    <label for="usertype">User Type</label>
                                    <select required class="form-control" id="usertype" name="usertype">
										<option value="1">
										CPS
										</option>
										<option value="2">
										CC CPS
										</option>
										
									</select>                                
								</div> 								
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input  class="form-control" type="file" id="image" name="image">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
function toggleUserType(val){
	if(val=='0'){
	   	$('#usertypeContainer').fadeOut();
	}else{
		$('#usertypeContainer').fadeIn();
	}
	
}


</script>
