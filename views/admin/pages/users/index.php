<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage Users';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Manage Users
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
                                    <th width="20%">Name</th>
                                    <th width="10%">Email</th>
                                    <th width="15%">Phone Number</th>
									<th width="20%">Profile Picture</th>
                                    <th width="10%">Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $results = User::all(['order' => 'user_id DESC']);
                                if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) { ?>
                                        <tr>
                                            <td width="5%"><?= $index++; ?></td>
                                            <td width="20%"><?= $row->user_name; ?></td>
                                            <td width="10%"><?= $row->user_email; ?></td>
                                            <td width="15%"><?= $row->user_phone; ?></td>											
                                            <td width="20%"><img
                                                        style="width:50px;height:50px;object-fit:cover;" class="img-circle"
                                                        src="<?php
                                                                    if(empty($row->user_image_type)){	
                                                                               echo $base_url."assets/admin/Ionicons/png/512/android-contact.png";											
                     				                               }else{
							                                             echo $base_url . $row->user_image . 'original.'. $row->user_image_type; 
								                                    	}
														?>"
                                                        alt="<?= $row->user_name; ?>"></td>
                                            <td width="10%">
                                                <?php if ($row->user_status == 1) { ?>
                                                    <button class="btn btn-success btn-xs" type="button"><i
                                                                class="fa fa-check"></i></button>
                                                <?php } else { ?>
                                                    <button class="btn btn-danger btn-xs" type="button"><i
                                                                class="fa fa-ban"></i></button>
                                                <?php } ?>
                                            </td>
                                            <td width="20%">
                                                <?php if ($row->user_status == 0) { ?>
                                                    <button class="btn btn-success btn-xs" type="button"
                                                            onclick="window.location.href='<?= $base_url . 'controllers/admin/user?action=status_on&user_id=' . encode_url($row->user_id) ?>'">
                                                        <i class="fa fa-check"></i></button>
                                                <?php } else { ?>
                                                    <button class="btn btn-danger btn-xs" type="button"
                                                            onclick="window.location.href='<?= $base_url . 'controllers/admin/user?action=status_off&user_id=' . encode_url($row->user_id) ?>'">
                                                        <i class="fa fa-ban"></i></button>
                                                <?php } ?>
                                                <button class="btn btn-primary btn-xs" type="button"
                                                        onclick="window.location.href='<?= $base_url . 'controllers/admin/user?action=edit&user_id=' . encode_url($row->user_id) ?>'">
                                                    <i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-danger btn-xs" type="button"
                                                        onclick="delete_object('<?= $base_url . 'controllers/admin/user?action=delete&user_id=' . encode_url($row->user_id) ?>')">
                                                    <i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
