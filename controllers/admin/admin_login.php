<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (isset($_GET['action']) && $_GET['action'] != 'logout') {
    if (admin_logged_in($_SESSION) == 1) {
        redirect($base_url . "admin/dashboard");
    } else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
        unset($_SESSION['admin']);
        $msg['errors'] = 'Your session cookie was expired. Please log in again.';
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/login");
    }
}

if (isset($_POST['login'])) {
    $validator = new Validator;
    $validation = $validator->validate($_POST, [
        'email' => 'required|email',
        'password' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/login");
    } else {
		$dd = User::find('all',['conditions'=>['user_status'=>1,'user_email'=>$_POST['email'],'user_pass'=>md5($_POST['password'])]]);
		if(count($dd) > 0){			
				$_SESSION['admin']['id'] = $dd[0]->user_id;								
				$_SESSION['admin']['name'] = $dd[0]->user_name;
				$_SESSION['admin']['phone'] = $dd[0]->user_phone;
				$_SESSION['admin']['email'] = $dd[0]->user_email;
				$_SESSION['admin']['created_at'] = $dd[0]->user_created_at;	
				$_SESSION['admin']['img'] = $dd[0]->user_image;
				$_SESSION['admin']['ext'] = $dd[0]->user_image_type;				
				$_SESSION['admin']['type'] = $dd[0]->user_desg;				
				$_SESSION['admin']['logged_in'] = TRUE;
				$_SESSION['admin']['ip_address'] = get_client_ip();
				$_SESSION['admin']['last_activity'] = time();
				$msg['success'] = "Login Successfully.";
				$_SESSION['admin']['msg'] = serialize($msg);
				redirect($base_url . "admin/dashboard");
		}else{
			$msg['errors'] = "No Record Found.";
			$_SESSION['admin']['msg'] = serialize($msg);
			redirect($base_url . "admin/login");
		}
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'logout') {
    unset($_SESSION['admin']);
    $msg['success'] = "Logout Successfully.";
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
} else {
    redirect($base_url . "admin/login");
}
?>