<?php
if (isset($msg['success'])) {
    echo '<div class="alert alert-success alert-dismissable fade in"> ';
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo "<strong> <i class='fa fa-check'></i> Congratulation! </strong>" . ucwords($msg['success']);
    echo '</div>';
}
if (isset($msg['errors'])) {
    echo '<div class="alert alert-danger alert-dismissable fade in"> ';
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo "<strong> <i class='fa fa-close'></i> Failed! </strong>" . ucwords($msg['errors']);
    echo '</div>';
}
if (isset($msg['warning'])) {
    echo '<div class="alert alert-warning alert-dismissable fade in"> ';
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo "<strong> <i class='fa fa-exclamation-triangle'></i> Warning! </strong>" . ucwords($msg['warning']);
    echo '</div>';
} ?>
