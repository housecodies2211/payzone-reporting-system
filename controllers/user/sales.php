<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add') {
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        's_daily_id' => 'required',
		's_customer_name' => 'required',
		's_service' => 'required',
		's_package' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sales/add");
    } else {
	      if(!@$_POST['s_daily_id'] || $_POST['s_daily_id']==0){
          $msg['errors'] = "Please Select The Activity Date";
          $_SESSION['admin']['msg'] = serialize($msg);
          redirect($base_url . "admin/sales/add"); 	
        }   		
		$data=Activities::all(['conditions'=>array('d_id'=>$_POST['s_daily_id'])]);
		$data1=count(SaleDetail::all(['conditions'=>array('s_daily_id'=>$_POST['s_daily_id'])]));
         		
        if($data[0]->d_sales>$data1){		 		
        $last_id = SaleDetail::last();
        if ($last_id != "") {
            $last_id = $last_id->s_id + 1;
        } else {
            $last_id = 1;
        }

        $saleDetail = new SaleDetail();
        $saleDetail->s_daily_id = $_POST['s_daily_id'];
        $saleDetail->s_customer_name = $_POST['s_customer_name'];
        $saleDetail->s_service = $_POST['s_service'];
        $saleDetail->s_package = $_POST['s_package'];
        $saleDetail->s_d_entry_date = $_POST['s_d_entry_date'];
		$saleDetail->s_user_id = $_SESSION['admin']['id'];
        $saleDetail->s_status = 1;
        $saleDetail->s_created_at = date('Y-m-d h:i:s');
        $saleDetail->s_updated_at = date('Y-m-d h:i:s');
        if ($saleDetail->save()) {
            $msg['success'] = "Sale Detail Added Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sales/add");
	   }else{
		  $msg['errors'] = "You Have Exceeded The Limit of Sale Detail For This Activity";
          $_SESSION['admin']['msg'] = serialize($msg);
          redirect($base_url . "admin/sales/add"); 		  
	   }
	   
	
	}
}
?>