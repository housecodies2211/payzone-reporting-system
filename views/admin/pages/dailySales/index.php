<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'View Sales';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                View Sales
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
									<th width="15%">Trading Name</th>
                                    <th width="15%">Lead Source</th>
                                    <th width="5%">Package ID</th>
									<th width="5%">Activity Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $results = SaleDetail::all(['conditions'=>array('s_user_id'=>$_SESSION['admin']['id']),'order' => 's_d_entry_date DESC']);
								if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) {
									?>
                                        <tr>
                                            <td width="5%"> <?= $row->s_id; ?></td>
											<td width="15%"><?= $row->s_customer_name; ?></td>
                                            <td width="15%"><?= $row->s_service; ?></td>
                                            <td width="10%"><?= $row->s_package; ?></td>
											<td width="10%"><?= date('d-m-Y',strtotime($row->s_d_entry_date)); ?></td>
                                        </tr>
                                    <?php
                                }}
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
