<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img style="height:45px;width: 45px;object-fit: cover;" src="<?php
                               if(empty($_SESSION['admin']['ext']) ){	
                                       echo $base_url."assets/admin/Ionicons/png/512/android-contact.png";											
                     				}else{
							               echo $base_url.$_SESSION['admin']['img'].'original.'.$_SESSION['admin']['ext'];
									}
				?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['admin']['name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?= (uri_segment($url,'dashboard'))?'active':''; ?>"><a href="<?= $base_url ?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <?php if($_SESSION['admin']['type']=='0'){?>
    		<li class="treeview <?= (uri_segment($url,'users'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Users</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'users'))?'active':''; ?>"><a href="<?= $base_url ?>admin/users/index"><i class="fa fa-dashboard"></i>Manage Users</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'users'))?'active':''; ?>"><a href="<?= $base_url ?>admin/users/add"><i class="fa fa-plus"></i>Add User</a></li>
                </ul>
            </li>
            <li class="<?= (uri_segment($url,'reports'))?'active':''; ?>">
                <a href="<?= $base_url ?>admin/reports/index">
                    <i class="fa fa-bar-chart"></i>
                    <span>See Activity Reports</span>
                </a>
            </li>
            <li class="<?= (uri_segment($url,'sale-reports'))?'active':''; ?>">
                <a href="<?= $base_url ?>admin/sale-reports/index">
                    <i class="fa fa-shopping-cart"></i>
                    <span>See Sale Reports</span>
                </a>
            </li>			
		  <?php } ?>
          <?php if($_SESSION['admin']['type']=='1'){?>
    		<li class="treeview <?= (uri_segment($url,'activities'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>Activities</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'activities'))?'active':''; ?>"><a href="<?= $base_url ?>admin/activities/index"><i class="fa fa-dashboard"></i>View Activities</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'activities'))?'active':''; ?>"><a href="<?= $base_url ?>admin/activities/add"><i class="fa fa-plus"></i>Add Activity</a></li>
                </ul>
            </li>
		  <?php } ?>
		  <?php if($_SESSION['admin']['type']=='1'){?>
    		<li class="treeview <?= (uri_segment($url,'sales'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Sales</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'sales'))?'active':''; ?>"><a href="<?= $base_url ?>admin/sales/index"><i class="fa fa-dashboard"></i>View Sales</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'sales'))?'active':''; ?>"><a href="<?= $base_url ?>admin/sales/add"><i class="fa fa-plus"></i>Add Sale</a></li>
                </ul>
            </li>
		  <?php } ?>
		    <li class="treeview <?= (uri_segment($url,'mailbox'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa fa-envelope"></i>
                    <span>Mail Box</span>
					<span class="label label-danger pull-right"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'mailbox'))?'active':''; ?>"><a href="<?= $base_url ?>admin/mailbox/index"><i class="fa fa-dashboard"></i>Inbox 					<span class="label label-danger pull-right"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span></a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'mailbox'))?'active':''; ?>"><a href="<?= $base_url ?>admin/mailbox/add"><i class="fa fa-plus"></i>Compose</a></li>
                </ul>
            </li>
		  <?php if(false){ ?>
            <li class="treeview <?= (uri_segment($url,'competitions'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i>
                    <span>Competitions</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'competitions'))?'active':''; ?>"><a href="<?= $base_url ?>admin/competitions/index"><i class="fa fa-dashboard"></i>Manage Competitions</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'competitions'))?'active':''; ?>"><a href="<?= $base_url ?>admin/competitions/add"><i class="fa fa-plus"></i>Add Competitions</a></li>
                </ul>
            </li>
             <li class="treeview <?= (uri_segment($url,'seasons'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-soccer-ball-o"></i>
                    <span>Seasons</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'seasons'))?'active':''; ?>"><a href="<?= $base_url ?>admin/seasons/index"><i class="fa fa-dashboard"></i>Manage Seasons</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'seasons'))?'active':''; ?>"><a href="<?= $base_url ?>admin/seasons/add"><i class="fa fa-plus"></i>Add Seasons</a></li>
                </ul>
            </li>
            <li class="treeview <?= (uri_segment($url,'teams'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-flag-o"></i>
                    <span>Teams</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'teams'))?'active':''; ?>"><a href="<?= $base_url ?>admin/teams/index"><i class="fa fa-dashboard"></i>Manage Teams</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'teams'))?'active':''; ?>"><a href="<?= $base_url ?>admin/teams/add"><i class="fa fa-plus"></i>Add Teams</a></li>
                </ul>
            </li>
            <li class="treeview <?= (uri_segment($url,'players'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-user-o"></i>
                    <span>Players</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'players'))?'active':''; ?>"><a href="<?= $base_url ?>admin/players/index"><i class="fa fa-dashboard"></i>Manage Players</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'players'))?'active':''; ?>"><a href="<?= $base_url ?>admin/players/add"><i class="fa fa-plus"></i>Add Players</a></li>
                </ul>
            </li>
            <li class="treeview <?= (uri_segment($url,'cities'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>Cities</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'cities'))?'active':''; ?>"><a href="<?= $base_url ?>admin/cities/index"><i class="fa fa-dashboard"></i>Manage Cities</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'cities'))?'active':''; ?>"><a href="<?= $base_url ?>admin/cities/add"><i class="fa fa-plus"></i>Add Cities</a></li>
                </ul>
            </li>
            <li class="treeview <?= (uri_segment($url,'venues'))?'active':''; ?>">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>Venue</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (uri_segment($url,'index') && uri_segment($url,'venues'))?'active':''; ?>"><a href="<?= $base_url ?>admin/venues/index"><i class="fa fa-dashboard"></i>Manage Venue</a></li>
                    <li class="<?= (uri_segment($url,'add') && uri_segment($url,'venues'))?'active':''; ?>"><a href="<?= $base_url ?>admin/venues/add"><i class="fa fa-plus"></i>Add venue</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/widgets.html">
                    <i class="fa fa-th"></i> <span>Widgets</span>
                    <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Charts</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>UI Elements</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Forms</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                    <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Tables</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                    <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/calendar.html">
                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                    <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
                </a>
            </li>
            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Examples</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                    <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                    <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                    <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                    <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                    <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                    <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                    <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Level One
                            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                </ul>
            </li>
            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
		  <?php }?>	
            <li><a href="<?= $base_url ?>admin/settings/index"><i class="fa fa-gears"></i> <span>Settings</span></a></li>
            <li><a href="<?= $base_url ?>admin/logout?action=logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>		  
		  </ul>
    </section>
    <!-- /.sidebar -->
</aside>