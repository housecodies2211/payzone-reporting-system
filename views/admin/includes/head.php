<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?= ucwords($author); ?>">
    <meta name="description" content="<?= ucwords($description); ?>">
    <meta name="keywords" content="<?= ucwords($keywords); ?>">
    <link rel="icon" href="<?= $base_url ?>assets/frontend/images/13.png" sizes="32x32" type="image/png">
    <title><?= ucwords($page_name); ?> | <?= ucwords($site_name); ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/morris.js/morris.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/adminlte/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?= $base_url; ?>assets/admin/adminlte/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
