<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Compose Email';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
	if(@$_GET['id']){
	$u_id=$_GET['id'];
    }else{
	$u_id='';	
	}

	?>

    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="<?= $base_url; ?>admin/mailbox/index" class="btn btn-primary btn-block margin-bottom">Back to Inbox</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?= $base_url; ?>admin/mailbox/index"><i class="fa fa-inbox"></i> Inbox
                  					<span class="label label-danger pull-right"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span></a></li>
                <li><a href="<?= $base_url; ?>admin/mailbox/sent"><i class="fa fa-envelope-o"></i> Sent</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
			<?php require_once $app_path . 'views/errors.php'; ?>		
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compose New Message</h3>
            </div>
            <!-- /.box-header -->
			<form role="form"
                       enctype="multipart/form-data"
                       id="form_validate" method="POST"
                       action="<?= $base_url ?>controllers/admin/mailbox?action=add&type=<?php echo $_SESSION['admin']['type'];?>">
            <div class="box-body">
              <div class="form-group">
			  <?php if($_SESSION['admin']['type']==0){?>
				<select name="m_id[]" id="m_id" class="form-control select2" multiple="multiple" data-placeholder="Select Emails"
                        style="width: 100%;">
                  <?php  
				       $results = User::all(['conditions'=>array('user_desg'=>1),'order' => 'user_id DESC']);
                                if (count($results) > 0) {
                                    foreach ($results as $row) {
                  ?>				
				  <option value="<?= $row->user_id?>" <?php if($u_id==$row->user_id){echo "selected='selected'";} ?>><?= $row->user_email?></option>
				  <?php } }?> 				  
				</select>
			  <?php }else{?>
				<select name="m_id[]" id="m_id" class="form-control select2" multiple="multiple" data-placeholder="Select Emails"
                        style="width: 100%;">
                  <?php  
				       $results = User::all(['conditions'=>array('user_desg'=>0),'order' => 'user_id DESC']);
                                if (count($results) > 0) {
                                    foreach ($results as $row) {
                  ?>				
				  <option value="<?= $row->user_id?>" <?php if($u_id==$row->user_id){echo "selected='selected'";} ?>><?= $row->user_email?></option>
				  <?php } }?> 				  
				</select>			  
			  
			  
			  <?php }?>
              </div>
              <div class="form-group">
                <input class="form-control" name="m_sub" placeholder="Subject:" />
              </div>
              <div class="form-group">
                    <textarea name="m_msg" id="compose-textarea" class="form-control" style="height: 300px"></textarea>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
            </div>
			</form>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    </div>
    
<?php
require_once $app_path . 'views/admin/includes/footer.php';
require_once $app_path . 'views/admin/includes/foot.php';
?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
$('.select2').select2()
</script>
