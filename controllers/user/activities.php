<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add') {
	
	$timestamp = strtotime($_POST['d_entry_date']);
    $day = date('D', $timestamp);
	if($day=="Sun" || $day=="Sat"){
		$msg['errors'] = "Oops! Saturday And Sunday Are Holidays"; 
	    $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add"); 	
	}	
	
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'd_appt_set_diary' => 'required',
		'd_appt' => 'required',
		'd_sales' => 'required',
		'd_cold_calls_completed' => 'required',
		'd_new_appt_made' => 'required',
		'd_entry_date' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");
    } else {
        $last_id = Activities::last();
        if ($last_id != "") {
            $last_id = $last_id->d_id + 1;
        } else {
            $last_id = 1;
        }
        $activity = new Activities();
		if(count(Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'],'d_entry_date'=>$_POST['d_entry_date']),'order' => 'd_entry_date DESC']))>0){
		$msg['errors'] = "You Already Entered This Activity If You want to edit Please Contact You Manager";
		$_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");	
		}
        $activity->d_appt_set_diary = $_POST['d_appt_set_diary'];
        $activity->d_appt = $_POST['d_appt'];
        $activity->d_sales = $_POST['d_sales'];
        $activity->d_cold_calls_completed = $_POST['d_cold_calls_completed'];		
        $activity->d_new_appt_made = $_POST['d_new_appt_made'];
        $activity->d_entry_date = $_POST['d_entry_date'];
		$activity->d_activity= $_POST['d_appt']+$_POST['d_new_appt_made'];
		$activity->d_conversion_rate_set_sat=($_POST['d_appt']/$_POST['d_appt_set_diary'])*100;
		$activity->d_conversion_rate_set_sold=($_POST['d_sales']/$_POST['d_appt'])*100;
        $activity->d_status = 1;
		$activity->d_sellday=1;
		$activity->d_user_id = $_SESSION['admin']['id'];
        $activity->d_created_at = date('Y-m-d h:i:s');
        $activity->d_updated_at = date('Y-m-d h:i:s');
        if ($activity->save()) {
            $msg['success'] = "Activity Created Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");
    }
}
?>