<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'See Reports';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
<link rel="stylesheet" href="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.css">
<style>
    .table-responsive>.fixed-column {
        position: absolute;
        display: inline-block;
        width: auto;
        border-right: 1px solid #ddd;
        background-color: #fff;
    }
    @media(min-width:768px) {
        .table-responsive>.fixed-column {
            display: none;
        }
    }
    .bg-red{
    	cursor: pointer;
    }
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    $date_range = date('01/m/Y') . ' - ' . date('d/m/Y');
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                See Reports
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="col-xs-12">
                            	<form action="<?= $base_url . 'admin/reports/index'; ?>" method="GET">
	                            	<div class="col-md-1 col-md-offset-2 col-sm-1">
		                            	<label>Searches</label>
	                            	</div>
	                            	<div class="col-md-3 col-sm-4">
	                            		<input type="text" class="form-control" name="user" value="<?= (isset($_GET['user']))?$_GET['user']:'';  ?>">
	                            	</div>
	                            	<div class="col-md-3 col-sm-4">
	                            		<input type="text" class="form-control" name="date_range" id="date_range" value="<?= (isset($_GET['date_range']))?$_GET['date_range']:$date_range;  ?>">
	                            	</div>
	                            	<div class="col-md-1 col-sm-2">
	                            		<input type="submit" class="btn btn-primary" name="Search" value="Search">
	                            	</div>
	                            	<div class="col-md-2 col-sm-12">
	                            		<button type="button" class="btn btn-danger pull-right" id="export_pdf"><i class="fa fa-file-pdf-o"></i></button>
		                        		<button type="button" class="btn btn-success pull-right" id="export_excel"><i class="fa fa-file-excel-o"></i></button>
	                            	</div>
                            	</form>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="table-responsive gen_pdf">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Summary</th>
                                            <?php
                                            if(isset($_GET['date_range'])){
                                            	$date_range = $_GET['date_range'];
                                            	$date_range = explode(' - ',$date_range);

                                            	$start_date = $date_range[0];
												$start_date = str_replace('/', '-', $start_date);
												$start_date = date('Y-m-d', strtotime($start_date));

												$end_date = $date_range[1];
												$end_date = str_replace('/', '-', $end_date);
												$end_date = date('Y-m-d', strtotime($end_date));
                                            }else{
                                            	$date_range = explode(' - ',$date_range);

                                            	$start_date = $date_range[0];
												$start_date = str_replace('/', '-', $start_date);
												$start_date = date('Y-m-d', strtotime($start_date));

												$end_date = $date_range[1];
												$end_date = str_replace('/', '-', $end_date);
												$end_date = date('Y-m-d', strtotime($end_date));
                                            }

                                            if(isset($_GET['user']) && $_GET['user'] != ""){
                                            	$user = $_GET['user'];
                                            	$all = User::find_by_sql('SELECT * FROM user WHERE user_desg = 1 AND user_name LIKE "%'.$user.'%" ORDER BY user_id DESC');
                                            }else{
                                            	$all = User::find_by_sql('SELECT * FROM user WHERE user_desg = 1 ORDER BY user_id DESC');
                                            }
                                            if (count($all) > 0) { 
                                                foreach ($all as $row) { ?>
                                                    <th><?= $row->user_name; ?></th>
                                                <?php }
                                            } ?>
                                            <th style="background-color: #00a65a !important;color:white;">Team Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $days_count = 0;
                                        $days = [
                                            'Mon' => 'Monday',
                                            'Tue' => 'Tuesday',
                                            'Wed' => 'Wednesday',
                                            'Thu' => 'Thursday',
                                            'Fri' => 'Friday',

                                        ];
                                        $modules = [
                                            'days',
                                            'Sell Day',
                                            'Appt set in Diary',
                                            'Appt Sat',
                                            'Sales',
                                            'Cold Calls Completed',
                                            'New Appts Made',
                                            'Daily Activity',
                                            'Conversion Rate Set to Sat',
                                            'Conversion Rate Sat to Sold',
                                        ];
                                    	
										$all_user_sum = [];
										$teamManage=[];
										$countSellDayForDaily=0;
										$countAppSatForDaily=2;
										$countNewAppForDaily=5;
										$countAppSatForSatSold=2;
										$countSalesForSatSold=3;
										$countAppSatForSetSat=2;
										$countSetForSetSat=1;
										$moduleCount=count($modules)-1;

	                                    while(strtotime($end_date. ' - '.$days_count.' day') >= strtotime($start_date) && count($all)>0){
	                                    	$new_class = uniqid();
	                                    	$dd = strtotime($end_date. ' - '.$days_count.' day');
											$nickDay = date('D',$dd);	 
	                                        if($nickDay=="Sun" || $nickDay=="Sat"){
	                                        	$days_count++;
	                                        }else{												
		                                        for($i=0;$i<sizeof($modules);$i++){ 
		                                        	if ($modules[$i]=='days'){ ?>
		                                                <tr style="background-color:red !important;" class="bg-red" onclick="hide_show('<?= $new_class; ?>')">
		                                                    <td  colspan="<?= count($all)+ 2; ?>"><?= $days[$nickDay] . "   " . date('F jS, Y',$dd); ?></td>
		                                                </tr>
		                                                <?php $days_count++; ?>
		                                            <?php }else{ ?>
		                                                <tr class="<?= $new_class; ?>">
		                                                    <td><?= $modules[$i]; ?></td>
		                                                    <?php if (count($all) > 0) { 
		                                                    	$row_total = 0;
																$yum=0;
		                                                        foreach ($all as $row) { 
		                                                        	$dact = Activities::find('all',['conditions'=>['d_user_id'=>$row->user_id,'d_entry_date'=>date('Y-m-d',$dd)]]);
		                                                        	if(count($dact) > 0){ 
		                                                        		?>
		                                                        		<?php if ($modules[$i] == 'Sell Day'){  ?>
				                                                            <td>
				                                                            	<?php
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_sellday);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_sellday);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_sellday);
				                                                            		echo round($dact[0]->d_sellday);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Appt set in Diary'){ ?>
				                                                            <td>
				                                                            	<?php
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_appt_set_diary);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_appt_set_diary);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_appt_set_diary);
				                                                            		echo round($dact[0]->d_appt_set_diary);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Appt Sat'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_appt);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_appt);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_appt);
				                                                            		echo round($dact[0]->d_appt);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Sales'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_sales);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_sales);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_sales);
				                                                            		echo round($dact[0]->d_sales);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Cold Calls Completed'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_cold_calls_completed);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_cold_calls_completed);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_cold_calls_completed);
				                                                            		echo round($dact[0]->d_cold_calls_completed);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'New Appts Made'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_new_appt_made);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_new_appt_made);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_new_appt_made);
				                                                            		echo round($dact[0]->d_new_appt_made);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Daily Activity'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_activity);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_activity);
				                                                            		}
				                                                            		$row_total += round($dact[0]->d_activity);
				                                                            		echo round($dact[0]->d_activity);
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Conversion Rate Set to Sat'){ ?>
		                                                        			<td>
				                                                            	<?php 
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_conversion_rate_set_sat);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_conversion_rate_set_sat);
				                                                            		}
																				    $yum++;
				                                                            		$row_total += round($dact[0]->d_conversion_rate_set_sat);
				                                                            		echo round($dact[0]->d_conversion_rate_set_sat) . '%';
			                                                            		?>
		                                                            		</td>
		                                                        		<?php }else if ($modules[$i] == 'Conversion Rate Sat to Sold'){ ?>
		                                                        			<td>
				                                                            	<?php
				                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
				                                                            			$all_user_sum[$row->user_id][$i] += round($dact[0]->d_conversion_rate_set_sold);
				                                                            		}else{
				                                                            			$all_user_sum[$row->user_id][$i] = round($dact[0]->d_conversion_rate_set_sold);
				                                                            		}
                                                                                    $yum++; 
				                                                            		$row_total += round($dact[0]->d_conversion_rate_set_sold);
				                                                            		echo round($dact[0]->d_conversion_rate_set_sold) . '%';
			                                                            		?>
		                                                            		</td>
			                                                            <?php } ?>
		                                                        	<?php }else{ 
	                                                            		if(isset($all_user_sum[$row->user_id][$i] )){
																				
	                                                            			$all_user_sum[$row->user_id][$i] += 0;
																			
	                                                            		}else{
	                                                            			$all_user_sum[$row->user_id][$i] = 0;
	                                                            		}
		                                                        		?>
			                                                            <td>N/A</td>
		                                                        	<?php }?>
		                                                        <?php }
		                                                    } ?>
															<td>
															<?php
															$teamManage[]=$row_total;
															

															
															if($modules[$i]=='Daily Activity'){
																			$sellDayVal=$teamManage[$countSellDayForDaily];
																			$newAppVal=$teamManage[$countNewAppForDaily];
																			$appSatVal=$teamManage[$countAppSatForDaily];
																			if($sellDayVal!=0)
																			echo round(($appSatVal+$newAppVal)/$sellDayVal,2);
																		    else echo "0";
																			$countSellDayForDaily+=$moduleCount;
																			$countNewAppForDaily+=$moduleCount;
																			$countAppSatForDaily+=$moduleCount;
																			
	                                                        }else if($modules[$i]=='Conversion Rate Sat to Sold'){
																			$appSatVal=$teamManage[$countAppSatForSatSold];
																			$salesMadeBen=$teamManage[$countSalesForSatSold];
																			if($appSatVal!=0)
																			echo round(($salesMadeBen/$appSatVal)*100)."%";
																		    else echo "0";
																			$countAppSatForSatSold+=$moduleCount;
																			$countSalesForSatSold+=$moduleCount;
																			
	                                                        }else if($modules[$i]=='Conversion Rate Set to Sat'){
																			$appSatVal=$teamManage[$countAppSatForSetSat];
																			$appSetVal=$teamManage[$countSetForSetSat];
																			if($appSetVal!=0)
																			echo round(($appSatVal/$appSetVal)*100)."%";
																		    else echo "0";
																			$countAppSatForSetSat+=$moduleCount;
																			$countSetForSetSat+=$moduleCount;
																			
	                                                        }
															
															else{
																
																echo $row_total;
															}
															
															?>
															</td> 
		                                                </tr>
		                                            <?php } 
		                                        	} 
			                                    }
			                                }
		                                ?>
										<?php //dd($teamManage);?>
                                    </tbody>
                                    <tfoot>
                                		<?php if(count($all) > 0){?>
	                                    	<tr style="background-color: #00a65a !important;color:white;">
	                                    		<td colspan="<?= count($all)+ 2; ?>"><strong>Total</strong></td>
	                                		</tr>
	                                		<?php for($i=0;$i<sizeof($modules);$i++){?>
											    
	                                			<?php if ($modules[$i]!='days'){ ?>
		                                			<tr>
			                                			<td><?= $modules[$i]; ?></td>
			                                			<?php if (count($all) > 0) { 
			                                				$all_user_total_sum  = 0;
			                                				foreach ($all as $row) { ?>
																<td><strong>
																	<?php 
																		$all_user_total_sum += $all_user_sum[$row->user_id][$i];
																		if($modules[$i]=='Sell Day'){
																		   echo $all_user_sum[$row->user_id][$i];
																		}
																		else if($modules[$i]=='Conversion Rate Sat to Sold'){
																			$j=$i-6;
																			$appSat2=$all_user_sum[$row->user_id][$j];
																			$j=$i-5;
																			$salesMade=$all_user_sum[$row->user_id][$j];
																			if($appSat2!=0)
																			echo round(($salesMade/$appSat2)*100)." %";
																		    else echo "0 %";
	                                                                    }																	
																		else if($modules[$i]=='Conversion Rate Set to Sat'){
																			$j=$i-6;
																			$appSet=$all_user_sum[$row->user_id][$j];
																			$j=$i-5;
																			$appSat1=$all_user_sum[$row->user_id][$j];
																			if($appSet!=0)
																			echo round(($appSat1/$appSet)*100)." %";
																		    else echo "0 %";
	                                                                    }
	                                                                    else if($modules[$i]=='Daily Activity'){
																			$j=$i-6;
																			$sellDay=$all_user_sum[$row->user_id][$j];
																			$j=$i-4;
																			$appSat=$all_user_sum[$row->user_id][$j];
																			$j=$i-1;
																			$newApp=$all_user_sum[$row->user_id][$j];
																			if($sellDay!=0)
																			echo round(($appSat+$newApp)/$sellDay,2);
																		    else echo "0";
	                                                                    }																		
																		else{
																			echo $all_user_sum[$row->user_id][$i];
																		}
																	?>
																</strong></td>
		                                					<?php }
			                                			}
			                                			?>
	                                					<td><strong>
	                                						<?php
															    $totalTeams[]=$all_user_total_sum;
																
	                                							if($modules[$i]=='Conversion Rate Set to Sat'){
																	
																	echo round(($totalTeams[2]/$totalTeams[1])*100). '%';
																}else if($modules[$i]=='Conversion Rate Sat to Sold'){
																	echo round(($totalTeams[3]/$totalTeams[2])*100). '%';
																}
																else if($modules[$i]=='Daily Activity'){
																	echo round(($totalTeams[2]+$totalTeams[5])/$totalTeams[0],2);
																}
																else{
																	echo $all_user_total_sum;
																}
	                            							?>	
	                            						</strong></td>
		    	                            		</tr>
		                            			<?php } ?>
	                            			<?php } ?>
                            			<?php } 
										?>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
	<script src="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?= $base_url; ?>assets/admin/table_export/tableExport.min.js"></script>
	<script src="<?= $base_url; ?>assets/admin/table_export/libs/js-xlsx/xlsx.core.min.js"></script>
	<script src="<?= $base_url; ?>assets/admin/table_export/libs/jsPDF/jspdf.min.js"></script>
	<script src="<?= $base_url; ?>assets/admin/table_export/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>

    <script>
        function hide_show(class_name){
        	$('.'+class_name).slideToggle('fast');
        }
        var tableToExcel = (function () {
		    var uri = 'data:application/vnd.ms-excel;base64,',
		        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		        , base64 = function (s) {
		            return window.btoa(unescape(encodeURIComponent(s)))
		        }
		        , format = function (s, c) {
		            return s.replace(/{(\w+)}/g, function (m, p) {
		                return c[p];
		            })
		        };
		    return function (table, name) {
		        if (!table.nodeType) table = document.querySelector(table);
		        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
		        window.location.href = uri + base64(format(template, ctx));
		    }
		})();
		$('body').on('click','#export_excel',function(){
			// $('.table').tableExport({
			// 	fileName: 'user-activities',
			// 	headers: true,
			// 	footers: true,
			// 	bootstrap: true,
			// 	type:'excel',
   //          	mso: {fileFormat:'xlsx',
   //              	worksheetName: ['User Activities']
   //          	} 
   //          });
   			tableToExcel('.table','date-to-date-report');
		});
		$('body').on('click','#export_pdf',function(){
			$('.table').tableExport({
				fileName: 'user-activities',
				type:'pdf',
				bootstrap: true,
               	jspdf: {orientation: 'l',
                       format: 'a4',
                       margins: {left:10, right:10, top:20, bottom:20},
                       autotable: {
                                   tableWidth: 'auto'}
                      }
            });
		});
	    $('#date_range').daterangepicker(
	      {
	      	maxDate: '<?= date("d/m/Y"); ?>',
	        timePicker: false, 
	        locale: {
		      format: 'DD/MM/YYYY'
		    }
	      },
	    );
    </script>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
