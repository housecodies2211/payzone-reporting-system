<?php
error_reporting(E_ALL);

$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || isset($_ENV['FORCE_HTTPS'])) ? 'https' : 'http';
$base_url .= '://' . $_SERVER['HTTP_HOST'] . '/';

$app_path = $_SERVER['DOCUMENT_ROOT'] . '/';

$url = $_SERVER['REQUEST_URI'];

$site_name = "Payzone Sales Transfer";

?>