<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Dashboard';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= $base_url ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <section class="content-header">
            <?php require_once $app_path . 'views/errors.php'; ?>
        </section>
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <?php if ($_SESSION['admin']['type'] == '0') { ?>
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3><?= count(User::all(['conditions' => array('user_desg' => 1), 'order' => 'user_id DESC'])); ?></h3>

                                <p>Total Users</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/users/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>Add</h3>

                                <p>New User</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/users/add" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3><?= count(User::all(['conditions' => array('user_desg' => 1, 'user_status' => 0), 'order' => 'user_id DESC'])); ?></h3>

                                <p>Blocked Users</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-ban"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/users/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>View</h3>

                                <p>Settings</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-gears"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                   
                </div>
                <!-- /.row -->
            <?php } ?>
			
			<div class="row">
			    <?php if ($_SESSION['admin']['type'] == '0') { ?>
                        <div class="col-md-6">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Users</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="sales-chart"
                                         style="height:height: 370px;; position: relative;"></div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-envelope"></i>

                                    <h3 class="box-title">Quick Email</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button type="button" class="btn btn-info btn-sm" data-widget="remove"
                                                data-toggle="tooltip"
                                                title="Remove">
                                            <i class="fa fa-times"></i></button>
                                    </div>
                                    <!-- /. tools -->
                                </div>
                                <div class="box-body">
                                    <form 
									 enctype="multipart/form-data"
                                     id="form_validate" method="POST"
                                     action="<?= $base_url ?>controllers/admin/mailbox?action=add&dash=1&type=<?php echo $_SESSION['admin']['type'];?>"
									
									>
                                        <div class="form-group">
												<select name="m_id[]" id="m_id" class="form-control select2" multiple="multiple" data-placeholder="Select Emails"
														style="width: 100%;">
												  <?php  
													   $results = User::all(['order' => 'user_id DESC']);
																if (count($results) > 0) {
																	foreach ($results as $row) {
												  ?>				
												  <option value="<?= $row->user_id?>"><?= $row->user_email?></option>
												  <?php } }?> 				  
												</select>
                                        </div>
									  <div class="form-group">
										<input class="form-control" name="m_sub" placeholder="Subject:" />
									  </div>
                                        <div>
                                        <textarea name="m_msg" class="textarea" placeholder="Message"
                                                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                   
                                </div>
                                <div class="box-footer clearfix">
                                    <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                                        <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
								 </form>
                            </div>
                        </div>
                    <?php } ?>
			
			</div>
            
            <?php if ($_SESSION['admin']['type'] == '1') { ?>
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>
								<?php
									 $activitySales=0;
                                     $activity=Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'])]);
                                     if(count($activity)>0){
									    foreach($activity as $s){
											$activityMonth=date("m", strtotime($s->d_entry_date));
											if($activityMonth==date('m')){
												$activitySales++;
											}
										}
                                       echo $activitySales;										
                                     }else echo "0"; 									 
 								?>
								</h3>

                                <p>Total Activities</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/activities/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>Add</h3>

                                <p>Activities</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-plus"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/activities/add" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>
									<?php
									 $countSales=0;
                                     $sale=SaleDetail::all(['conditions'=>array('s_user_id'=>$_SESSION['admin']['id'])]);
                                     if(count($sale)>0){
									    foreach($sale as $s){
											$saleMonth=date("m", strtotime($s->s_d_entry_date));
											if($saleMonth==date('m')){
												$countSales++;
											}
										}
                                       echo $countSales;										
                                     }else echo "0"; 									 
 									?>								
								</h3>

                                <p>Total Sales</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>View</h3>

                                <p>Settings</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-gears"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
            <?php } ?>
        </section>
    </div>
    
    <?php
    
    $total = count(User::find('all', ['conditions' => ['user_desg' => 1]]));
    $active = count(User::find('all', ['conditions' => ['user_status' => 1, 'user_desg' => 1]]));
    $non_active = count(User::find('all', ['conditions' => ['user_status' => 0, 'user_desg' => 1]]));
    
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
$('.select2').select2()
</script>