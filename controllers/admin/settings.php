<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'update') {
    if (isset($_POST['user_id']) && $_POST['user_id'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
        'name' => 'required',
		'password'=>'min:6',
        'image' => 'uploaded_file:0,5M,png,jpeg,jpg,gif',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/settings/index");
        } else {
            $id = $_POST['user_id'];
            $check = User::find(['conditions' => ['user_id' => $id]]);
            if ($check != "") {
                if ($_FILES['image']['name'] != "" && isset($_FILES['image']['name'])) {
                    $sizing = array();
                    $image = upload_image('image', $app_path, 'uploads/user_images/' . $id, $sizing);
			        if($_SESSION['admin']['id']==$id){
                    $_SESSION['admin']['img']= $file_name = $image['file_name'];
					$_SESSION['admin']['ext']= $ext = $image['ext'];
					}else{$file_name = $image['file_name'];$ext = $image['ext'];}
                    
                } else {
                    $file_name = $check->user_image;
                    $ext = $check->user_image_type;
                }
				$check->user_name = $_POST['name'];
				if(@$_POST['password'])
				$check->user_pass = md5($_POST['password']);
				$check->user_phone = $_POST['phone'];		
				$check->user_image = $file_name;
				$check->user_image_type = $ext;
				$check->user_updated_at = date('Y-m-d h:i:s');
                if ($check->save()) {
				$_SESSION['admin']['name'] = $_POST['name'];
				$_SESSION['admin']['phone'] = $_POST['phone'];
                    $msg['success'] = "Profile Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/settings/index");
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/settings/index");
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/settings/index");
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/settings/index");
    }
}  else {
    redirect($base_url . "admin/dashboard");
}
?>