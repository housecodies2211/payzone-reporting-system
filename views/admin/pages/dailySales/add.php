<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Add Sale Detail';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Sale Detail
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="activity_validate" method="POST"
                              action="<?= $base_url ?>controllers/user/sales?action=add">
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="box-body">
							    <div class="form-group">
                                <label for="s_daily_id">Select Activity ID</label>
                                      <select class="form-control" required name="s_daily_id" id="s_daily_id">
                                      <option value="0">Select Activity Date</option>									  
							    <?php
                                      $results = Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id']),'order' => 'd_entry_date DESC']);
								      foreach($results as $res){  
								?> 
                                     
									      <option value="<?= $res->d_id; ?>" date="<?= $res->d_entry_date; ?>"><?= "# ".$res->d_id." (".$res->d_entry_date.") "; ?></option>
								<?php }?>		  
									 </select>
                                </div>								
								<input type="hidden" id="s_d_entry_date" name="s_d_entry_date" value=""/>
                                <div class="form-group">
                                    <label for="s_customer_name">Trading Name</label>
                                    <input required type="text" class="form-control" name="s_customer_name"
                                              id="s_customer_name"
                                              placeholder="Enter Trading Name" />
                                </div>
                                <div class="form-group">
                                    <label for="s_service">Lead Source</label>
									<select class="form-control" required name="s_service" id="s_service">
                                       <option value="1">Cold Call</option>
                                       <option value="2">Tele Sales</option>
                                       <option value="3">Referral</option> 									
                                    </select>									
                                </div>						
                                <div class="form-group">
                                    <label for="s_package">Package ID</label>
                                    <input required type="text"  class="form-control" name="s_package"
                                              id="s_package"
                                              placeholder="Enter Package ID" />
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
$('#s_daily_id').on('change', function() {
	var value=$(this).val();
	var getDate=$(this).children('option[value='+value+']');
    $("#s_d_entry_date").val(getDate.attr("date"));
});


	
</script>